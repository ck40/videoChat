var express = require('express');
var router = express.Router();
var OpenTok = require('opentok');


/* GET home page. */

/*
router.get('/', function(req, res, next) {
  //res.render('index', { title: 'Express' });

});
*/

//45933932 apiKey
//daa4a88157091f5b56a4f2b9debebb5c0c1bf4c9 //secrect
//1_MX40NTkzMzkzMn5-MTUwMjE3MjYyNTY3MX5uZlZZcFl1ZXJTTzExMTliRmR4SldYT0Z-fg //sessionId
/* token publisher
 T1==cGFydG5lcl9pZD00NTkzMzkzMiZzaWc9MjY0Njk3MjhmYTEwMTJiNzMxNmI1ZjJhZDgxYmE3NWJmYWNmYjFlNDpzZXNzaW9uX2lkPTFfTVg0ME5Ua3pNemt6TW41LU1UVXdNakUzTWpZeU5UWTNNWDV1WmxaWmNGbDFaWEpUVHpFeE1UbGlSbVI0U2xkWVQwWi1mZyZjcmVhdGVfdGltZT0xNTAyMTcyNzQxJm5vbmNlPTAuNjcyMDQxMjQ5NjQwODM3MiZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNTA0NzY0NzQwJmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9
 */
/* token subscriber
 T1==cGFydG5lcl9pZD00NTkzMzkzMiZzaWc9MmQwMDliYWI2M2MyMGFkZjVjMjkzMWU0ZGRjNDA2OTYyZmUyMTkyODpzZXNzaW9uX2lkPTFfTVg0ME5Ua3pNemt6TW41LU1UVXdNakUzTWpZeU5UWTNNWDV1WmxaWmNGbDFaWEpUVHpFeE1UbGlSbVI0U2xkWVQwWi1mZyZjcmVhdGVfdGltZT0xNTAyMTcyODA3Jm5vbmNlPTAuNTEwNzI1MjI0OTI1MjY3OCZyb2xlPXN1YnNjcmliZXImZXhwaXJlX3RpbWU9MTUwNDc2NDgwNSZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==
 */


var token ="";
var opentok=null;
const apiKey='45933932';
const apiSecret='daa4a88157091f5b56a4f2b9debebb5c0c1bf4c9';
var sessionSrv=null;

router.get('/createSession', function(req, res, next) {
    //
    //var apiKey=req.body.apiKey;
    //var apiSecret=req.body.apiSecret;
    //if(apiKey && apiSecret) {
        opentok = new OpenTok(apiKey,apiSecret);
        //opentok = new OpenTok(apiKey, apiSecret);
        // Create a session that will attempt to transmit streams directly between
        // clients. If clients cannot connect, the session uses the OpenTok TURN server:
        if(session && token!="" ){
            res.status(200).send({sessionId:sessionSrv.sessionId,apiKey:apiKey,token:token});
        }else{

        }
        opentok.createSession(function (err, session) {
            sessionSrv=_.cloneDeep(session);
            if (err)  {
                res.status(500).send(err);
                return console.log(err);
            }//return console.log(err);

            // Generate a Token from a session object (returned from createSession)
            token = session.generateToken();

            // Set some options in a Token
            token = session.generateToken({
                role: 'moderator',
                expireTime: (new Date().getTime() / 1000) + (7 * 24 * 60 * 60), // in one week
                data: 'name=Kfir',
                initialLayoutClassList: ['focus']
            });
            res.status(200).send({sessionId:session.sessionId,apiKey:session.ot.apiKey,token:token});
            return console.log('session Created');
            // save the sessionId
            //db.save('session', session.sessionId, done);
        });
        /*
    } else {
        res.status(400).send();
    }
    res.status(200).end();
    */
});



router.post('/forceDisconnect', function(req, res, next) {
    var sessionId = req.body.sessionId;
    var connectionId = req.body.connectionId;
    if (opentok && sessionId && connectionId) {
        opentok.forceDisconnect(sessionId, connectionId, function (error) {
            if (error) res.status(500).send(error); //return console.log("error:", error);
            res.status(200).send('disconnect succed');
        });
    }else{
        res.status(400).send();
    }

})



module.exports = router;
